import React from 'react';
import ReactDOM from 'react-dom';
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap/dist/js/bootstrap.js';
import './css/home.css';


var Nav = React.createClass({
    render: function(){
        return (
            <nav className="navbar-custom">
              <div className="container-fluid">
                <div className="navbar-header">
                  <button type="button" className="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span className="icon-bar" style={{background:"white"}}></span>
                    <span className="icon-bar" style={{background:"white"}}></span>
                    <span className="icon-bar" style={{background:"white"}}></span>
                  </button>
                  <a className="navbar-brand" href="/">
                      <img src="images/logo_white.png" alt="Nomad Pass"></img>
                  </a>
                </div>
                <div className="collapse navbar-collapse">
                  <ul className="nav navbar-nav navbar-right">
                    <li><a className="nav-link" href="/">Community</a></li>
                    <li><a className="nav-link" href="/">Destinations</a></li>
                    <li><a className="nav-link" href="/">Remote Guide</a></li>
                    <li><a className="nav-link" href="/">About</a></li>
                    <li><a className="nav-link" href="/">Log In</a></li>
                    <li><a className="nav-link-signup" href="/">Sign Up</a></li>
                  </ul>
                </div>
              </div>
            </nav>
        )
    }
});

var MainBanner = React.createClass({
    render: function(){
        return (
            <div className="main-banner">
                <div className="main-content">
                    <img src="/images/icon-logo.png" style={{"maxWidth":"50px"}}>
                    </img>
                    <h1>Work Different</h1>
                    <p>
                        Your personalized solution to finding the best places to live and work remotely and to meeting digital nomads anywhere in the world
                    </p>
                    <button className="btn-get-started" type="button">Get Started</button>
                    <div className="nextArrow">
                        <a href="#nomadPlaces">
                            <img src="/images/arrow.png"></img>
                        </a>
                    </div>
                </div>
            </div>
        )
    }
});

var NomadPlaces = React.createClass({
    render: function(){
        return (
            <div className="container nomad-places">
                <div className="row">
                    <h2>Nomad Pass is the platform for everyone working remotely</h2>
                    <p>Get your pass to connect with other digital nomads and find unique places to live and work anywhere.</p>
                    <div className="col-sm-12">
                        <div className="container grid-places">
                            <div className="row">
                                <a className="col-xs-12 col-sm-8" href="/">
                                    <figure>
                                        <img src="images/live-and-work-in-panama.jpg" alt="man dives in the water from the tropical destination of cocovivo panama during work break."></img>
                                        <figcaption><span className="title">Live &amp; work in a Tropical Destination</span><span className="location">in Panama</span></figcaption>
                                    </figure>
                                </a>
                                <a className="col-xs-12 col-sm-4" href="/">
                                    <figure>
                                        <img src="images/freelancer-anna-taking-a-picture.jpg" alt="anna taking a picture."></img>
                                        <figcaption><span className="title">Meet Anna </span><span className="location">Freelance Designer in London</span></figcaption>
                                    </figure>
                                </a>
                            </div>
                            <div className="row">
                                <a className="col-xs-12 col-sm-4" href="/">
                                    <figure>
                                        <img src="images/surf-with-digital-nomads-dave-in-bali.jpg" alt="dave with surf board infront of a van."></img>
                                        <figcaption><span className="title">Surf with Dave</span><span className="location">in Bali</span></figcaption>
                                    </figure>
                                </a>
                                <a className="col-xs-12 col-sm-4" href="/">
                                    <figure>
                                        <img src="images/be-a-local-in-lisbon.jpg" alt="experience life as a local going up the hills with a tram in lisbon."></img>
                                        <figcaption><span className="title">Experience Life as a Local</span><span className="location">in Lisbon</span></figcaption>
                                    </figure>
                                </a>
                                <a className="col-xs-12 col-sm-4" href="/">
                                    <figure>
                                        <img src="images/digital-nomads-cowork-together-from-tropical-coworking-space-in-koh-lanta.jpg" alt="cowork with other digital nomads at kohub, tropical coworking space in the island of koh lanta."></img>
                                        <figcaption><span className="title">Co-Work</span><span className="location">in Koh Lanta</span></figcaption>
                                    </figure>
                                </a>
                            </div>
                        </div>
                        <div className="row">
                            <button type="button" className="btn-explore-more">Explore More</button>
                        </div>
                    </div>
                </div>

            </div>
        )
    }
});

var Explore = React.createClass({
    render: function(){
        return (
            <div className="explore">
                <h2>Ready to explore Nomad Pass?</h2>
                <div className="container">
                    <div className="row">
                        <div className="col-sm-12 col-md-6">
                            <h3>“Working remotely doesn’t have to be lonely.”</h3>
                            <p>
                                Our interactive community recommends you cool people that match with your location, interests, skills & travel plans. Make new friends, collaborate & create business opportunities on the road.
                            </p>
                            <a href="/" className="explore-link"><u>Explore</u> &gt;</a>
                        </div>
                        <div className="col-sm-12 col-md-6">
                            <img src="/images/laptop-screenshot-of-community-platform.png"></img>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
});

var Footer = React.createClass({
    render: function(){
        return (
            <div className="page-footer">
                <div className="container">
                    <div className="row">
                        <div className="col-xs-12">
                            <div className="social-links">
                                <a className="email" href="/" title="Email">Email</a>
                                <a className="fb" href="/" title="Facebook">Facebook</a>
                                <a className="tw" href="/" title="Twitter">Twitter</a>
                                <a className="ig" href="/" title="Instagram">Instagram</a>
                                <a className="in" href="/" title="Linkedin">linkedin</a>
                                <a className="pi" href="/" title="Pinterest">Pinterest</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
});

ReactDOM.render(
    <Nav />,
    document.getElementById('nav'));

ReactDOM.render(
    <MainBanner />,
    document.getElementById('mainBanner'));

ReactDOM.render(
    <NomadPlaces />,
    document.getElementById('nomadPlaces'));

ReactDOM.render(
    <Explore />,
    document.getElementById('explore'));


ReactDOM.render(
    <Footer />,
    document.getElementById('pageFooter'));
