# What is this ?
This repo contains the code which implements the front page as given by the
design specification using ReactJS.

# How to run the app ?
- Open a terminal and cd to the project folder.
- Run `npm install` to install all the dependencies.
- Run `npm start` to -
    - Pack the modules into **bin/bundle.js** using webpack.
    - Launch the Express server (used to serve all the files).
- Open `http://localhost:3000` in browser to access the site.
- To stop the server hit 'Ctrl + C'.
