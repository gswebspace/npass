var express = require('express');
var path = require('path');
var app = express();

var server_port = process.env.PORT || 3000;

app.use(express.static('./bin'));
app.use("/images", express.static('./images'));
app.use('/', function (req, res) {
    if(req.path === "/"){
        res.sendFile(path.resolve('src/index.html'));
    }
});

app.listen(server_port, function(error) {
  if (error) throw error;
    console.log("--->Server listening on port", server_port);
});
